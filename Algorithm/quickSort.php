<?php
/**
 * 快读排序.
 */

function quickSort($arr) {

    if (count($arr) > 1) {
        $key = $arr[0];

        $x = [];
        $y = [];

        for ($i = 1; $i < count($arr); $i++) {
            if ($arr[$i] < $key) {
                $x[] = $arr[$i];
            } else {
                $y[] = $arr[$i];
            }
        }

        file_put_contents("result.txt", "**************************** \n", FILE_APPEND);
        file_put_contents("result.txt", "x = ".print_r($x, true), FILE_APPEND);
        file_put_contents("result.txt", "y = " . print_r($y, true), FILE_APPEND);

        $x = quickSort($x);
        $y = quickSort($y);

        return array_merge($x, [$key], $y);
    } else {
        return $arr;
    }
}

$arr = [1,5,2,4,3,6,9,7,8,0];
$arr1 = quickSort($arr);

print_r($arr1);