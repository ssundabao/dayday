<?php
/**
 * 选择排序.
 */
function selectionSort($arr) {
    $len = count($arr);

    if ($len > 1) {

        for ($i = 0; $i < $len - 1; $i++) {
            $min = $i;
            for ($j = $i + 1; $j < $len; $j++) {
                if ($arr[$min] > $arr[$j]) {
                    $min = $j;
                }
            }
            if ($arr[$min] < $arr[$i]) {
                $swap = $arr[$min];
                $arr[$min] = $arr[$i];
                $arr[$i] = $swap;
            }
        }

    }

    return $arr;
}

$arr = [1,5,2,4,3,6,9,7,8,0];
$arr1 = selectionSort($arr);

print_r($arr1);