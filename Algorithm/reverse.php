<?php
/**
 * 数组反转.
 * User: ltt
 * Date: 2018/2/22
 * Time: 下午6:16
 */
function reverse($arr) {

    $len = count($arr);
    if ($len <= 1) {
        return $arr;
    }
    $left = 0;
    $right = $len - 1;
    while ($left < $right) {
        $temp = $arr[$left];
        $arr[$left] = $arr[$right];
        $arr[$right] = $temp;
        $left++;
        $right--;
    }

    return $arr;
}

$arr = [1,2,3,4,5,6,7,8,9];
$arr1 = reverse($arr);
print_r($arr1);

