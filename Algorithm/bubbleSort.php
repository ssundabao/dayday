<?php

/**
 * 冒泡排序
 */
function bubbleSort($arr) {

    $len = count($arr);

    if ($len > 1) {

        for ($i = 0; $i < $len; $i++) {
            for ($j = 0; $j< $len - $i - 1; $j++) {

                if ($arr[$j] > $arr[$j + 1]) {
                    $temp = $arr[$j];
                    $arr[$j] = $arr[$j + 1];
                    $arr[$j + 1] = $temp;
                }

            }
        }
    }

    return $arr;
}


$a = [3,5,1,2,9,4,6,7];
$b = bubbleSort($a);

print_r($b);