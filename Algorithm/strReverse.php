<?php
/**
 * 字符串反转.
 * User: Jack
 * Date: 2018/2/28
 * Time: 下午6:19
 */
//strrev
$newStr = strrev($str);

//倒序遍历
function strReverse($str) {
    $len = strlen($str);

    if ($len <= 1) {
        return $str;
    }

    $newStr = '';
    for ($i = $len-1; $i >= 0; $i--) {
        $newStr .= $str[$i];
    }

    return $newStr;
}

// 首尾倒置
function strReverse1($str) {
    $len = strlen($str);

    if ($len <= 1) {
        return $str;
    }

    $left = 0;
    $right = $len - 1;
    while ($left < $right) {
        $temp = $str[$left];
        $str[$left] = $str[$right];
        $str[$right] = $temp;
        $left++;
        $right--;
    }

    return $str;

}

// 处理汉字
function strReverseMb($str) {
    $revStr = '';
    if (is_string($str) && mb_check_encoding($str, "UTF-8")) {
        $len = mb_strlen($str);

        for ($i = $len-1; $i>=0; $i--) {
            $revStr .= mb_substr($str, $i, 1, "UTF-8");
        }
    }
    return $revStr;
}

echo(strReverseMb($str));

