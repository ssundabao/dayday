<?php
/**
 * 遍历目录下所有文件和子目录.
 * User: Jack
 * Date: 2018/2/28
 * Time: 下午8:21
 */
//opendir + readdir
function getFiles($dir) {
    if (!is_dir($dir)) {
        return null;
    }
    $files = [];

    $handle = opendir($dir);

    if (!empty($handle)) {
        while (false !== ($file = readdir($handle))) {
            $fullpath = $dir . '/' . $file;
            if ($file != '.' && $file != '..') {
                if (is_dir($fullpath)) {
                    $subFiles = getFiles($fullpath);

                    $files[] = array_merge($files, $subFiles);
                } else {
                    $files[] = $fullpath;
                }
            }
        }
    }
    closedir($handle);

    return $files;
}

//glob
function getFilesGlob($dir) {
    $files = [];
    if (!is_dir($dir)) {
        return $files;
    }

    $dir = realpath($dir);
    $pattern = $dir . "/*";
    $filesArr = glob($pattern);

    foreach ($filesArr as $file) {
        if (is_dir($file)) {
            $subFiles = getFiles($file);
            if (is_array($subFiles)) {
                $files = array_merge($files, $subFiles);
            }
        } else {
            $files[] = $files;
        }
    }

    return $file;
}

//dir
function getFilesDir($dir) {
    $files = [];
    if (is_dir($dir)) {
        $dir = dir($dir);
        if ($dir) {
            while (false !== ($file = $dir->read())) {
                if ($file != "." && $file != "..") {
                    if (is_dir($file)) {
                        $fullPath = $dir . '/' . $file;
                        $subFiles = getFilesDir($fullPath);
                        $files = array_merge($files, $subFiles);
                    } else {
                        $files[] = $file;
                    }
                }
            }
        }
    }
    $dir->close();
    return $files;
}

print_r(getFiles("/Users/Jack/Code/jack/learn"));