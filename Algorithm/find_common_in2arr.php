<?php
/**
 * 寻找两个有序数组里相同的元素.
 * User: Jack
 * Date: 2018/2/22
 * Time: 下午8:24
 * @param $arr1
 * @param $arr2
 * @return array
 */
function find_common_in2arr($arr1, $arr2) {
    $common = [];
    while (!empty($arr1) && !empty($arr2)) {
        if ($arr1[0] > $arr2[0]) {
            array_shift($arr2);
        } else if($arr1[0] < $arr2[0]) {
            array_shift($arr1);
        } else {
            $common[] = array_shift($arr1);
            array_shift($arr2);
        }
    }
    return $common;
}

$a1 = [1,2,4,6,7,8];
$a2 = [2,4,6,7,9,10];
$common = find_common_in2arr($a1, $a2);
print_r($common);