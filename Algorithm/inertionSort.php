<?php
/**
 * 插入排序.
 */
function insertionSort($arr) {
    $len = count($arr);

    for ($i = 1; $i < $len; $i++) {
        $temp = $arr[$i];

        file_put_contents("result.txt", "**************************** \n", FILE_APPEND);

        $j = $i - 1;
        while ($temp < $arr[$j]) {
            $arr[$j + 1] = $arr[$j];
            $arr[$j] = $temp;
            $j--;
            if ($j < 0) {
                break;
            }

            file_put_contents("result.txt", "i=$i j=$j \n" . print_r($arr, true), FILE_APPEND);
        }

        file_put_contents("result.txt", "arr => " . print_r($arr, true), FILE_APPEND);

    }

    return $arr;
}

$arr = [1,5,2,4,3,6,9,7,8,0];
$arr1 = insertionSort($arr);
print_r($arr1);

