<?php
/**
 * 数组顺序打乱.
 * User: Jack
 * Date: 2018/2/22
 * Time: 下午10:20
 * @param $arr
 * @return array
 */
function array_shuffle($arr) {

    $len = count($arr);
    if ($len <= 1) {
        return $arr;
    }

    for ($i = 0; $i < $len-1; $i++) {
        $rand = rand(0, $len -1);
        $temp = $arr[$rand];
        $arr[$rand] = $arr[$i];
        $arr[$i] = $temp;
    }

    return $temp;
}