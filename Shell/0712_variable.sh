#/bin/sh
#
# 2016-07-12

# 定义变量
name="jack"
age=23
echo ${name} # echo 1
echo $age # echo 2

# readonly
readonly $name # 只读
name="jet"
age=24
echo ${name} # echo 1
echo $age # echo 2

# unset
unset age # 
echo ${name} # echo 1
echo $age # echo 2

# 单引号
sex='男' # 单引号,原样输出
echo $sex

# 双引号
echo "I'm $name." # 双引号  可以包含变量

# 拼接字符串
echo "I'm "$name"!" # 拼接字符串 1
echo "I'm ${name}!" # 拼接字符串 2
echo $name $sex # 拼接字符串 3

# 获取字符串长度
echo ${#name}

# 截取子字符串
echo ${name:1:1}
# 查找子字符串
#echo `expr "$name" is`

# 数组
person={$name,$sex}
person['age']=26
echo ${person['name']}
echo ${person[@]}

# 取得数组元素的个数
echo ${#person[*]}


