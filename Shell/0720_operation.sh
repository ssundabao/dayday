#!/bin/bash
a=6
b=3
c=`expr $a + $b` # 加号俩边必须有空格
d=`expr $a - $b`
e=`expr $a \* $b`
f=`expr $a / $b`
g=`expr $a % $b`
echo 'a+b='$c
echo 'a-b='$d
echo 'a*b='$e
echo 'a/b='$f
echo 'a%b='$g
if [ $a == $b ]
	then
	echo 'a == b'
fi
if [ $a != $b ]
	then
	echo 'a != b'
fi

if [ $a -eq $b ]; then
 	echo 'a == b'
fi 

if [ $a -ne $b ]; then
 	echo 'a != b'
fi 

if [ $a -gt $b ]; then
 	echo 'a > b'
fi

if [ $a -lt $b ]; then
 	echo 'a < b'
fi

if [ $a -ge $b ]; then
 	echo 'a >= b'
fi

if [ $a -le $b ]; then
 	echo 'a <= b'
fi

if [ ! $a -le $b ]; then
 	echo 'a >= b'
fi