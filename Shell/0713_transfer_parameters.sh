#/bin/sh

echo "SHELL 传递参数实例！"
echo $1 # 输出第一个参数

echo $# # 输出参数个数

echo $$ # 输出当前脚本运行的ID号

echo $! # 后台运行的最后一个进程的ID号

echo $* # 输出所有参数

echo $@ # 输出所有参数

echo $- # 显示Shell使用的当前选项

echo $? # 显示最后命令的退出状态。0表示没有错误，其他任何值表明有错误。

for i in $*;do # 循环输出参数
	echo $i
done 

for i in "$*";do # 加双引号，所有参数被当做一个参数
	echo $i
done 

for i in $@;do
	echo $i
done 
