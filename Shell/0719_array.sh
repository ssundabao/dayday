#/bin/sh

my_array=(a b 'c' d)

echo "第一个元素:${my_array[0]}"
echo "第二个元素:${my_array[1]}"
echo "第三个元素:${my_array[2]}"
echo "第四个元素:${my_array[3]}"

echo "数组元素:${my_array[*]}"
echo "数组元素:${my_array[@]}"

echo "数组元素个数:${#my_array[*]}"
echo "数组元素个数:${#my_array[@]}"