-- 大概就是 mysql的一张表存放着某个用户的余额，下面我写伪代码了。

-- $sql=select * from user wherer 余额 > 10 and id=1;
-- //如果有余额进行逻辑处理最后在减去扣款。
-- if($sql){
-- echo '请求它接口';
-- $sql=update user set 余额=余额-10;//关键就在这里了，如果减完了就相当于没余额了，但是由于并发，第二个人查询的时候是有的，他也进了这个条件。怎么避免这种情况

-- }else{

-- echo '余额不足';
-- }

1、悲观锁
start transaction
select ... for update // 锁数据
...
update ... // 更新锁住数据的状态
commit

2、乐观锁

原理

首先cas token其实就是一个版本号，

1，我取一条数据时，会顺带返回一个版本号($casToken)给我：$data = $m->get('ip_block', null, $casToken);

2，然后我修改$data后，再回存这个$data时，我需要靠这个版本号才能存回去:$m->cas($casToken, 'ip_block', $data);

如果1、2过程之间已经有人存了$data，那么由于版本号$casToken已经发生变化，第2步就会失败。


上面是mysql本身的机制，另外也可以采用自己实现的乐观锁，即
数据库增加一个version字段

select ...,verison as last_version form table where condition
...
update table set ...,version=version+1 where condition and version = last_version