# 表table
year	dept
2008	dev
2008	sale
2009	dev
2010	hr
2010	man
2010	market
2011	service

# 结果 写一个sql实现以下查询结果：
year	dept
2008	dev,sale
2009	dev
2010	hr,man,market
2011	service

# sql
select year,group_concat(dept) from `table` group by year;