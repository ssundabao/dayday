# 一张表，id class score，一条SQL语句，获得每个班级大于60分和小于60分的个数，结果为:
#班级 大于60 小于60
#A1 2 3
#A2 1 2
# sql
select class as '班级',count(score>60) as '大于60',count(score<60) as '小于60' from student group by class;