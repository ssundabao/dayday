# student
# courseid	coursename	score
1		java			70
2		oracle			90
3		xml				40
4		jsp				30
5		servlet			80
# 为了方便阅读，查询此表后的显示结果如下（及格分数为60分）：
courseid	coursename	score	mark
1			java			70	pass
2			oracle			90	pass
3			xml				40	fail
4			jsp				30	fail
5			servlet			80	pass

# sql
select *,case when score>60 then pass else fail end as mark from student;